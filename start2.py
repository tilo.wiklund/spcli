#!/bin/env python

##### WARNING: This script is *almost* as messy and incomprehensible as
##### Studentportalen itself. If you need it for something even remotely
##### serious drop me an email and I will help you extract the important parts
##### for you.

import readline
from cmd import Cmd
import magic
import math
import os
import tempfile
import bbcode
import re
import time
import frontmatter
import yaml
from tqdm import tqdm
import shutil
import requests
from requests_toolbelt.multipart import encoder
import json
import getpass
import pick
import itertools
import pandocfilters
from itertools import groupby,cycle,chain
from bs4 import BeautifulSoup
import IPython
from urllib.parse import urlparse,urlunparse,parse_qs,quote,urlencode
import urllib.parse
import pypandoc
import stat
import sys

run_in_debug = True
# run_in_debug = False

bb = bbcode.Parser(escape_html=False, replace_links=False)
# bb.add_simple_formatter('size', '<span style="font-size:%(size)spx>%(value)s</span>')

#### URL translation stuff

def parse_teacher_url(url):
    path_pattern_read = re.compile("^/portal/(authsec/)?portal/uusp/((?:admin-courses)|(?:student))(/.*)$")
    urlp = urlparse(url)
    (authsec, teacher, subpath) = re.match(path_pattern_read, urlp.path).groups()
    qs = parse_qs(urlp.query)

    return (authsec or "", teacher, subpath, qs)

#TODO: Rewrite this in terms of urlunparse
def unparse_teacher_url(authsec_new, teacher_new, subpath_new, qs_new, keys=None):
    keys = keys or set(qs_new.keys())
    return "https://studentportalen.uu.se/portal/{}portal/uusp/{}{}?{}".format(authsec_new, teacher_new, subpath_new, "&".join("{}={}".format(k,quote(qs_new[k][0], safe='')) for k in qs_new if k in keys))

def student_from_teacher_forum(url):
    (authsec, teacher, subpath, qs) = parse_teacher_url(url)

    forum_view_keys = {'entityId', 'toolAttachmentId', 'toolMode'} # set.intersection(*(set(parse_qs(urlparse(url).query).keys()) for url in forum_urls))
    forum_post_keys = { 'action',
                        'entityId',
                        'location',
                        'mode',
                        'requestURI',
                        'toolAttachmentId',
                        'toolMode',
                        'webwork.portlet.action',
                        'webwork.portlet.eventAction',
                        'webwork.portlet.mode',
                        'webwork.portlet.portletNamespace',
                        'windowstate'
                        } #set.intersection(*(set(parse_qs(urlparse(url).query).keys()) for url in post_urls))
    forum_extra_keys = set({ })

    forum_keys = forum_view_keys.union(forum_post_keys).union(forum_extra_keys)

    authsec_new = authsec
    teacher_new = "student"
    subpath_new = subpath

    if ('toolMode' in qs) and qs['toolMode'][0] in ['use', 'view']:
        qs['toolMode'] = ['studentUse']

    return unparse_teacher_url(authsec_new,
                          teacher_new,
                          subpath_new,
                          qs, forum_keys)

def student_from_teacher_filearea(url):
    (authsec, teacher, subpath, qs) = parse_teacher_url(url)

    filearea_keys = { 'action',
                      'entityId',
                      'location',
                      'mode',
                      'requestURI',
                      'toolAttachmentId',
                      'toolMode',
                      'webwork.portlet.action',
                      'webwork.portlet.eventAction',
                      'webwork.portlet.mode',
                      'webwork.portlet.portletNamespace',
                      'windowstate' }

    authsec_new = authsec
    teacher_new = "student"
    subpath_new = subpath

    if ('toolMode' in qs) and qs['toolMode'][0] in ['use', 'view']:
        qs['toolMode'] = ['studentUse']

    return unparse_teacher_url(authsec_new,
                          teacher_new,
                          subpath_new,
                          qs, filearea_keys)

def student_from_teacher_content(url):
    (authsec, teacher, subpath, qs) = parse_teacher_url(url)

    # content_keys = {'action',
    #                 'entityId',
    #                 'location',
    #                 'mode',
    #                 'requestURI',
    #                 'toolAttachmentId',
    #                 'toolMode',
    #                 'webwork.portlet.action',
    #                 'webwork.portlet.eventAction',
    #                 'webwork.portlet.mode',
    #                 'webwork.portlet.portletNamespace',
    #                 'windowstate'
    #                 }

    authsec_new = authsec
    teacher_new = "student"
    subpath_new = subpath

    qs_new = { 'toolMode' : ['studentUse'],
               'uusp.portalpage' : ['true'],
               'toolAttachmentId' : qs['toolAttachmentId'] }


    return unparse_teacher_url(authsec_new,
                          teacher_new,
                          subpath_new,
                          qs_new)

def force_student_url(url):
    if urlparse(url).path.startswith("/portal"):
        (authsec, teacher, subpath, qs) = parse_teacher_url(url)

        if teacher == "student":
            if run_in_debug:
                print("Already a student url: {}".format(url))
            return url
        elif not teacher == "admin-courses":
            raise RuntimeError("This does not look like a teacher/student url: {}".format(url))

        if subpath.startswith("/forum"):
            return student_from_teacher_forum(url)
        elif subpath.startswith("/filearea"):
            return student_from_teacher_filearea(url)
        elif subpath.startswith("/webpage"):
            return student_from_teacher_content(url)
        else:
            raise RuntimeError("Unknown URL-type: {}".format(url))

    elif urlparse(url).path.startswith("/uusp-webapp/auth/webwork/jforum/attachment.action"):
        return url
    else:
        raise RuntimeError("Unknown URL-type: {}".format(url))

#### Main Stuff

def pick_all(name, xs):
    return xs

def pick_manual(name, xss):
    items = [ (cat, x) for cat,xs in xss for x in xs ]
    labels = [ "{} : {}".format(cat, x.title()) for cat,xs in xss for x in xs ]
    selected = pick.pick( labels,
                          "Parts of {} to download".format(name),
                          multi_select=True )
    return [ (cat, [y[1] for y in ys]) for cat,ys in itertools.groupby([ items[i] for lab,i in selected ], lambda x: x[1])]
    # return [ xs[i] for text,i in selected ]

class WithCallback:
    def __init__(self, onenter, onexit):
        self._onenter = onenter
        self._onexit  = onexit

    def __enter__(self):
        f = self._onenter
        return f()

    def __exit__(self, tp, val, tr):
        f = self._onexit
        return f()

## Studentportalen only allows one post every 20 seconds, this is some
## hacky (global) book keeping to avoid posting too often
class PostLock:
    def __init__(self, timeout=20):
        self._timeout = timeout
        now = time.time()
        self._run = now-self._timeout-1

    def lock(self, message=None):
        this = self

        def onenter():
            now = time.time()
            diff = now - this._run
            if diff < this._timeout:
                verbose_wait(math.ceil(this._timeout - diff), message)

        def onexit():
            now = time.time()
            this._run = now

        return WithCallback(onenter, onexit)

global_post_lock   = PostLock(timeout = 21)
global_upload_lock = PostLock(timeout = 2)

try:
    # PyInstaller creates a temp folder and stores path in _MEIPASS
    base_path = sys._MEIPASS
except Exception:
    base_path = os.path.abspath(".")

pandoc_path = os.path.join(base_path, 'pandoc/pandoc')

if not os.path.exists(pandoc_path):
    print("Pandoc not found!")
    sys.exit(1)

# all_permissions = stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IWOTH | stat.S_IXOTH
# os.chmod(pandoc_path, all_permissions)

os.environ.setdefault('PYPANDOC_PANDOC', pandoc_path)

## TODO: We should really make the URL/URIs proper types...
def parse_sp_requestUri(uri):
    new_uri = None

    try:
        post_num,forum_uri = re.match("^/uusp-webapp/posts/list(?:/([0-9]+))?/([0-9]+).page$", uri).groups()
        if post_num:
            new_uri = ('forum_post', post_num, forum_uri)
        else:
            new_uri = ('forum_topic', forum_uri)
        return new_uri
    except:
        new_uri = None

    try:
        if uri == '':
            return ('empty',)
    except:
        new_uri = None

    new_uri = uri
    if run_in_debug:
        print("Unknown URI {}".format(uri))
        IPython.embed()

    return new_uri

def parse_sp_url(url):
    urlp = urlparse(url)
    qs = parse_qs(urlp.query)
    # TODO: Make sure these are enough
    return (urlp.netloc, urlp.path,
            parse_sp_requestUri(qs.setdefault('requestURI',[''])[0]),
            qs.setdefault('mode', [''])[0],
            qs.setdefault('nodeId', [''][0]))

def verbose_wait(sec, message="Waiting"):
    [time.sleep(1) for i in tqdm(range(sec), desc=message)]

def relative(req, path):
    parsed_uri = urlparse(req.url)
    return '{uri.scheme}://{uri.netloc}{uri.path}{path}'.format(uri=parsed_uri,path=path)

def chunksof(xs, n, head=0, tail=0):
    for i in range(head, len(xs)-tail-n+1, n):
        yield xs[i:(i+n)]

def matches(pattern, text):
    m = pattern.search(text)
    offset = 0
    while m:
        f,t  = m.span()
        yield (f+offset, t+offset)
        offset = offset + t
        m = pattern.search(text[offset:])

def slide2(itr):
    try:
        a = itr.__next__()
        while True:
            b = itr.__next__()
            yield (a, b)
            a = b
    except StopIteration:
        return

def apply_seqs(ranges, text, on_match, on_rest=None):
    ms = slide2(itertools.chain([0], (x for y in ranges for x in y), [len(text)]))
    funs = cycle([on_rest, on_match])
    for (a,b),f in zip(ms, funs):
        if a != b:
            if f:
                yield f(text[a:b])
            else:
                yield text[a:b]

def absolute(req, path):
    parsed_uri = urlparse(req.url)
    return '{uri.scheme}://{uri.netloc}{path}'.format(uri=parsed_uri,path=path)

def bs(req):
    return BeautifulSoup(req.content, 'html.parser')

def bslxml(req):
    return BeautifulSoup(req.content, 'lxml')

def extract_current_menu(req):
    def utterly_stupid_current(tag):
        if not tag.has_attr('data'):
            return False
        fields = { fs[0]:fs[1] for fs in [field.strip().split(':') for field in tag.get('data').split(',')] }
        if not 'addClass' in fields:
            return False
        classes = fields['addClass'].strip("'").split(' ')
        return ('menuTreeCurrent' in classes) or ('menuCurrent' in classes)

    return absolute(req, bslxml(req).find(utterly_stupid_current).find('a').get('href'))
        # base1.find('li', { 'data' : "addClass:'menuTreeCurrent'" }) or \
        # base1.find('li', { 'data' : "addClass:'toolreg menuTreeCurrent'" }) or \
        # base1.find('li', { 'data' : "addClass:'menuCurrent'" })

def assert_exists(x):
    assert(not (x is None))
    return x

def assert_unique(x):
    assert(len(x) == 1)
    return x[0]

def assert_equal_length(xs, ys):
    assert(len(xs) == len(ys))

def new_name(where, what, forbid_suffixes=['\.metadata']):
    i = 1
    suffix = ''
    what = re.sub('$', '', what)
    while any([ re.match('{}$'.format(forbidden), what+suffix) for forbidden in forbid_suffixes ]) or \
          os.path.exists(os.path.join(where, what+suffix)):
        suffix = " ({})".format(i)
        i = i+1
    return what + suffix

def fuzzy_match(left, right):
    words = re.split('\W+', left)
    for word in words:
        try:
            right = right[right.index(word)+len(word):]
        except ValueError:
            return False
    return True
    # return all([word in right for word in words])

def login(username, password):
    s    = requests.Session()

    r1   = s.get("https://studentportalen.uu.se")
    doc1 = bs(r1)

    login_link = doc1.select('#loginLink')[0].get('href')

    r2   = s.get(relative(r1, login_link))#r1.url + login_link)
    doc2 = bs(r2)

    login_action = doc2.select('#login')[0].get('action')

    r3 = s.post(absolute(r2, login_action), allow_redirects=True,
                data = { 'j_username' : username, 'j_password' : password, '_eventId_proceed' : ''})
    doc3 = bs(r3)

    login_error = doc3.select('p.form-error')
    if login_error:
        print("Unable to login: \n {}".format("\n".join([ e.text for e in login_error ])))
        sys.exit(1)
    else:
        return s

class Item:
    def __init__(self, title):
        self._title = title

    def ls(self):
        return []

    def flat_ls(self):
        return [a for (t, b) in self.ls() for a in b ]

    def title(self):
        return self._title

    def remote_url(self):
        return None

    def load_self(self, path):
        return

    def sort(self):
        return "Unknown"

    def put(self, s, other):
        if(isinstance(other, Item)):
            print("Error: I do not know how to put {} into a {}, skipping {}".format(self.sort(), other.sort(), other.title()))
            return []
        else:
            print("Error: I do not know how to put {} this into a {}, skipping".format(type(other), self.sort()))
            return []

    def is_browsable(self):
        return False

    def force_sync(self, s):
        return self

    def sync(self, s):
        return self

    def deep_sync(self, s):
        return self.sync(s)

    def local(self):
        raise NotINotImplementedError()

    def save(self, where, selector=pick_all):
        print("Writing {}".format(self.title()))
        name = new_name(where, self.title())
        path = os.path.join(where, name)
        with open(path, 'w') as f:
            f.write(self.title())
        return { path : self }

class BrowsableItem(Item):
    def __init__(self, title):
        super().__init__(title)

    def is_browsable(self):
        return True

    def deep_sync(self, s):
        raise NotImplementedError()

    def save(self, where, selector=pick_all):
        print("Writing {}".format(self.title()))
        name = new_name(where, self.title())
        path = os.path.join(where, name)
        os.makedirs(path)
        files = { path : self }
        for item in [ x for cat,xs in selector(self.title(), self.ls()) for x in xs ]:
            # import pdb; pdb.set_trace()
            files.update(item.save(path, selector))
        return files

class LocalBrowsable(BrowsableItem):
    def __init__(self, title, items):
        super().__init__(title)
        self._items = items

    def deep_sync(self, s):
        self._items = [ (cat, [item.deep_sync(s) for item in items_]) for (cat, items_) in self._items ]
        return self

    def local(self):
        return True

    def ls(self):
        return self._items

class RemoteItem(Item):
    def local(self):
        return False

    def force_sync(self, s):
        return self.sync(s)

    def sync(self, s):
        raise NotImplementedError()

    def save(self, where, selector=pick_all):
        # TODO: Figure out the correct way to handle this
        raise NotImplementedError()

# TODO: Make this a mixin or something
class RemoteBrowsable(BrowsableItem):
    def local(self):
        return False

    def force_sync(self, s):
        return self.sync(s)

    def deep_sync(self, s):
        return self.sync(s).deep_sync(s)

    def sync(self, s):
        raise NotImplementedError()

    def save(self, where, selector=pick_all):
        # TODO: Figure out the correct way to handle this
        raise NotImplementedError()

class LocalUnknownPage(Item):
    def __init__(self, title, body, remote = None):
        super().__init__(title)
        self._body   = body
        self._remote = remote

    # TODO: This is an ugly hack, figure out good semantics
    def remote_url(self):
        if self._remote and self._remote._url:
            return self._remote._url

    def sort(self):
        return "page of unknown type"

    def force_sync(self, s):
        return self._remote.force_sync(s)

    def deep_sync(self, s):
        return self.sync(s)

    def save(self, where, selector=pick_all):
        print("Writing {}".format(self.title()))
        name = new_name(where, self.title())
        path = os.path.join(where, name)
        with open(path, "w") as f:
            f.write(str(self._body))
        return { path : self }

class LocalContentPage(Item):
    def __init__(self, title, text_sv, text_en, remote = None):
        super().__init__(title)
        self._text_sv = text_sv
        self._text_en = text_en
        self._remote  = remote

    def put(self, s, other):
        print("Error: Cannot put things into a content page, skipping {}".format(other.title()))
        return []
        # if not self._remote:
        #     print("Error: Not a remote location, skipping {}".format(self.title()))
        #     return []
        # else:
        #     return self._remote.put(s, other)

    # TODO: This is an ugly hack, figure out good semantics
    def remote_url(self):
        if self._remote and self._remote._url:
            return self._remote._url

    def force_sync(self, s):
        return self._remote.force_sync(s)

    def sort(self):
        return "content page"

    def deep_sync(self, s):
        return self.sync(s)

    def save(self, where, selector=pick_all):
        print("Writing {}".format(self.title()))
        name = new_name(where, self.title())
        path = os.path.join(where, name)
        os.makedirs(path)

        path_se = os.path.join(path, 'se')
        path_en = os.path.join(path, 'en')

        with open(path_se, 'w') as f_se:
            post_se = frontmatter.Post(self._text_sv)
            post_se['title'] = self.title()
            f_se.write(frontmatter.dumps(post_se))
        with open(path_en, 'w') as f_en:
            post_en = frontmatter.Post(self._text_en)
            post_en['title'] = self.title()
            f_en.write(frontmatter.dumps(post_en))

        return { path : self, path_se : (), path_en : () }

def sp_parse_file_area(r, file_area):
    explore = file_area.find('form', { 'name' : 'fa_explore' }).find('table', { 'id' : 'filesTable' })
    table_head = [x.text.strip() for x in explore.find('thead').find_all('th')]

    name_col = table_head.index('Name')
    size_col = table_head.index('Size')
    uptd_col = table_head.index('Updated')

    files = []
    table_body = explore.find('tbody')
    if table_body.text.strip() != "This folder is empty.":
        for file_row in table_body.find_all('tr'):
            file_cols = file_row.find_all('td')
            file_main = file_cols[name_col]
            file_name = file_main.find_all('a')[1].text.strip()
            # TODO: Description lies after a <br/>
            file_url  = absolute(r, file_main.find('a').get('href'))
            file_fldr = file_main.find('img').get('src').endswith('folder.gif')
            file_size = file_cols[size_col].text.strip()
            file_uptd = file_cols[uptd_col].text.strip()
            files = files + [{ 'name' :    file_name,
                               'url' :     file_url,
                               'size' :    file_size,
                               'updated' : file_uptd,
                               'folder' :  file_fldr  }]
    return files

def diff_file_areas(r1, area1, r2, area2):
    files1 = sp_parse_file_area(r1, area1.find('div', { 'class' : 'filearea' }))
    files2 = sp_parse_file_area(r2, area2.find('div', { 'class' : 'filearea' }))

    return ([ f for f in files1 if parse_sp_url(f['url']) not in [ parse_sp_url(g['url']) for g in files2 ]],
            [ f for f in files2 if parse_sp_url(f['url']) not in [ parse_sp_url(g['url']) for g in files1 ]])

def hidden_form_fields(form):
    return { h.get('name') : h.get('value') for h in form.find_all('input', { 'type' : 'hidden' }) }

# NOTE: We need to reload the page each time to get a cross-site safety (csrf) token
# WARNING: Studentportalen supports multiple files with the same name, this
# function does not check for conflicts
def sp_upload_file(s, url, path):
    if not os.path.exists(path):
        print("Could not find {}".format(path))
        return False
    elif not os.path.isfile(path):
        print("{} is not a file".format(path))
        return False

    r   = s.get(url)
    doc = bs(r)

    upload_form = doc.find('form', { 'name' : 'fa_upload' })
    if not upload_form:
        print("Error: Unable to upload file (not a Studentportalen file area?)")
        return False

    action = upload_form.get('action')

    fields = hidden_form_fields(upload_form)
    file_field_name = upload_form.find('input', { 'type' : 'file' }).get('name')
    # TODO: Add metadata support
    fields[file_field_name] = (os.path.basename(path), open(path, 'rb'), magic.from_file(path, mime=True))

    total_size = os.path.getsize(path)
    print("Uploading {}...".format(path))
    pbar = tqdm(total = total_size, unit="b", unit_scale=True)

    class PBarCallback:
        last = 0
        def __call__(self, monitor):
            pbar.update(monitor.bytes_read - self.last)
            self.last = monitor.bytes_read

    enc = encoder.MultipartEncoderMonitor(encoder.MultipartEncoder(fields=fields), PBarCallback())

    headers = { 'Content-Type' : enc.content_type }

    # with global_upload_lock.lock("Waiting to upload"):
    r2 = s.post(absolute(r, action), data=enc, headers = headers)
    pbar.close()

    doc2 = bs(r2)

    del_files, add_files = diff_file_areas(r, doc, r2, doc2)

    assert(len(del_files) == 0)
    assert(len(add_files) == 1)

    return add_files[0]

# NOTE: We need to reload the page each time to get a cross-site safety (csrf) token
# WARNING: Studentportalen supports multiple folders with the same name, this
# function does not check for conflicts
def sp_make_folder(s, url, name):
    r   = s.get(url)
    doc = bs(r)

    folder_form = doc.find('form', { 'name' : 'fa_add' })
    if not folder_form:
        print("Error: Unable to create folder (not a Studentportalen file area?)")
        return

    action = folder_form.get('action')

    fields = hidden_form_fields(folder_form)
    name_field_name = folder_form.find('input', { 'name' : 'title' })
    fields[name_field_name] = name

    r2 = s.post(absolute(r, action), data=fields)

    doc2 = bs(r2)

    del_files, add_files = diff_file_areas(r, doc, r2, doc2)

    assert(len(del_files) == 0)
    assert(len(add_files) == 1)

    return add_files[0]

def form_select_values(select):
    return { opt.text.strip() : opt.get('value') for opt in select.find_all('option') }

def sp_add_function(s, url, sort, form_fields):
    r = s.get(url)
    doc = bs(r)

    add_url = absolute(r, doc.find('a', { 'class' : ' addFunction' }).get('href'))

    r2 = s.get(add_url)
    doc2 = bs(r2)

    create_form = doc2.find('form', {'id' : 'createToolRegistrations' })

    fields = hidden_form_fields(create_form)
    # fields['toolType'] = form_select_values(create_form.find('select', { 'name' : 'toolType' }))[sort]
    fields['toolType'] = sort

    action = absolute(r2, create_form.get('action'))

    r3 = s.post(action, data=fields)
    doc3 = bs(r3)

    final_form = doc3.find('div', { 'class' : 'functionForm' }).find('form', { 'name' : 'form' })
    action = absolute(r3, final_form.get('action'))

    # Studentportalen is completely non-functional as usual, for some reason
    # there are necessary hidden fields outside the form...
    final_form = doc3.find('div', { 'class' : 'functionForm' }).find('form', { 'name' : 'form' }).parent.parent
    fields2 = hidden_form_fields(final_form.parent.parent)
    fields2.update(form_fields)

    return s.post(action, data=fields2)

def follow_final_link(s, r):
    doc = bs(r)

    return s.get(assert_unique([ absolute(r, l.get('href')) for l in doc.find('div', { 'class' : 'actionLinks' }).find_all('a')
                                 if l.text.strip() in ['Open', 'Öppna'] ]))

def sp_add_forum(s, url, name, name_en = None, draft=True):
    fields = { 'labelSv' : name,
               'labelEn' : name_en or name }

    if draft:
        fields['status'] = 1
    else:
        fields['status'] = 3

    r = follow_final_link(s, sp_add_function(s, url, 'forum', fields))

    # TODO: make suer that 'draft' and 'public' are the correct strings
    # if draft:
    #     return LocalForum(name, [], SPPage(name, r.url, 'draft'))
    # else:
    #     return LocalForum(name, [], SPPage(name, r.url, 'public'))

    return r.url

def sp_add_file_area(s, url, name, description='', description_en=None, name_en = None, draft=True):
    fields = { 'labelSv' :       name,
               'labelEn' :       name_en or name,
               'descriptionSv' : description,
               'descriptionEn' : description_en or description,
               'toolProfileId' : 1 }

    if draft:
        fields['status'] = 1
    else:
        fields['status'] = 3

    r = follow_final_link(s, sp_add_function(s, url, 'filearea', fields))

    # TODO: make suer that 'draft' and 'public' are the correct strings
    # if draft:
    #     return LocalFolder(name, [], SPPage(name, r.url, 'draft'))
    # else:
    #     return LocalFolder(name, [], SPPage(name, r.url, 'public'))
    return r.url

def sp_add_content_page(s, url, name, html='', html_en=None, name_en = None, draft=True):
    fields = { 'labelSv' :       name,
               'labelEn' :       name_en or name,
               'htmlContentSv' : html,
               'htmlContentEn' : html_en or html }

    if draft:
        fields['status'] = 1
    else:
        fields['status'] = 3

    r = sp_add_function(s, url, 'webpage', fields)

    # Studentportalen is, as usual, a completely inexplicable mess, for some
    # reason we need to extract the link as a permanent link for students, that
    # we translate into a teacher link and later translate back into a student
    # link.
    parsed_url = list(urlparse(bslxml(r).find('div', { 'class' : 'imageToolPermalink' }).find('a', { 'href' : '#' }).get('onclick')[13:-2]))
    parsed_qs = parse_qs(parsed_url[4])
    parsed_qs['toolMode'] = ['view']
    parsed_url[4] = urlencode(parsed_qs, doseq=True)
    parsed_url[2] = re.sub('/uusp/student/', '/uusp/admin-courses/', parsed_url[2])
    return urlunparse(parsed_url)

    # return r.url

    # TODO: make suer that 'draft' and 'public' are the correct strings
    # if draft:
    #     return LocalContentPage(name, html, html_en or html, SPPage(name, r.url, 'draft'))
    # else:
    #     return LocalContentPage(name, html, html_en or html, SPPage(name, r.url, 'public'))

# NOTE: For some reason this works only with the lxml parser
def sp_parse_forum_area(r, forum_area):
    forum_topic_rows = forum_area.find_all('tr', recursive=False)[1:-1]

    forum_topics = []
    for row in forum_topic_rows:
        topic_cols  = row.find_all('td', recursive=False)

        topic_sorts = topic_cols[0].find('img').get('class')
        topic_sort  = 'Unknown'
        if 'icon_folder_announce' in topic_sorts:
            topic_sort = 'Announce'
        elif 'icon_folder_sticky' in topic_sorts:
            topic_sort = 'Sticky'
        elif 'icon_folder' in topic_sorts:
            topic_sort = 'Post'

        topic_subject   = topic_cols[1].text.strip()
        topic_url       = absolute(r, topic_cols[1].find('a').get('href'))
        topic_responses = int(topic_cols[2].text.strip())
        topic_author    = topic_cols[3].text.strip()
        # NOTE: topic_cols[4] is last post info

        forum_topics = forum_topics + [{ 'subject' :   topic_subject,
                                         'url' :       topic_url,
                                         'responses' : topic_responses,
                                         'author' :    topic_author,
                                         'sort' :      topic_sort }]

    return forum_topics

def sp_find_javascript_redirects(doc):
    return [ m.group(1).encode('ascii').decode('unicode-escape')
             for m in (re.match("document\.location='([^']+)'", script.text.strip()) for script in doc.find_all('script'))
             if m is not None ]

# TODO: WARNING: The returned URL is not reliable, Studentportalen has some
# strange indirect ways of triggering (javascript) redirects that I can't
# always detect, it does seem to work after posting a new topic though...
def sp_do_forum_post(s, url, subject, text, attachments = [], sort = None):
    r = s.get(url)
    doc = bs(r)

    post_form = doc.find('form', { 'name' : 'post' })

    post_action = absolute(r, post_form.get('action'))

    # NOTE: Hidden fields added right before call since we may have to redo the
    # post
    fields = {} #hidden_form_fields(post_form)

    fields['subject'] = subject

    # fields['disable_html'] = None
    fields['disable_bbcode'] = None # 'on'

    if sort:
        if sort == 'Post':
            fields['topic_type'] = '0'
        elif sort == 'Sticky':
            fields['topic_type'] = '1'
        elif sort == 'Announce':
            fields['topic_type'] = '2'
        else:
            raise NotImplementedError()

    if len(attachments) > 3:
        raise NotImplementedError()

    fields['total_files'] = str(len(attachments))

    if run_in_debug and any([ not isinstance(attach, str) for attach in attachments ]):
        print("Trying to attach something other than a path")
        ipython.embed()

    total_size = sum([ os.path.getsize(f) for f in attachments ])
    for i,f in enumerate(attachments):
        # fields['file_{}'.format(i)] = (os.path.basename(f), open(f, 'rb'), magic.from_file(f, mime=True))
        fields['comment_{}'.format(i)] = ''

    fields['message'] = text

    if attachments:
        def do_the_post(docf):
            fieldsf = fields.copy()
            fieldsf.update(hidden_form_fields(docf.find('form', { 'name' : 'post' })))

            for i,f in enumerate(attachments):
                fieldsf['file_{}'.format(i)] = (os.path.basename(f), open(f, 'rb'), magic.from_file(f, mime=True))

            print("Uploading attachments...")
            pbar = tqdm(total = total_size, unit="b", unit_scale=True)

            # Closures are a bit finicky in Python...
            class PBarCallback:
                last = 0
                def __call__(self, monitor):
                    pbar.update(monitor.bytes_read - self.last)
                    self.last = monitor.bytes_read

            enc = encoder.MultipartEncoderMonitor(encoder.MultipartEncoder(fields=fieldsf), PBarCallback())

            headers = { 'Content-Type' : enc.content_type }

            rf = s.post(post_action, data=enc, headers = headers)
            pbar.close()

            return rf
    else:
        def do_the_post(docf):
            fieldsf = fields.copy()
            fieldsf.update(hidden_form_fields(docf.find('form', { 'name' : 'post' })))

            return s.post(post_action, data=fieldsf)

    with global_post_lock.lock("Waiting to repost"):
        ri = do_the_post(bs(r))
    doci = bslxml(ri)
    while True:
        redirects = sp_find_javascript_redirects(doci)
        new_form = doci.find('form', { 'name' : 'post' })
        if redirects:
            ri = s.get(absolute(ri, assert_unique(redirects)))
            doci = bslxml(ri)
            continue
        elif new_form is None or new_form.find('font', { 'color' : 'red' }):
            with global_post_lock.lock("Waiting to repost"):
                ri = do_the_post(doci)
            doci = bslxml(ri)
            continue
        else:
            break
    return ri

def diff_forum_posts(posts1, posts2):
    l = [ p for p in posts1 if not p in posts2 ]
    r = [ p for p in posts2 if not p in posts1 ]
    return (l, r)

def sp_post_forum_post(s, url, subject, text, attachments = [], quiet = False):
    r = s.get(url)
    doc = bs(r)

    forum_area     = assert_exists(doc.find('table', { 'class' : 'forumline' }))
    original_posts = sp_parse_forum_topic(s, r, forum_area)

    reply_link = absolute(r, doc.find('a', { 'class' : 'icon_reply' }).get('href'))

    attachments       = attachments[:3]
    other_attachments = attachments[3:]
    if other_attachments and not quiet:
        print("Warning: Studentportalen does not support attaching more than three files, making multiple posts")

    r2         = sp_do_forum_post(s, reply_link, subject, text, attachments)
    doc2       = bslxml(r2)
    reply_link = absolute(r2, doc2.find('a', { 'class' : 'icon_reply' }).get('href'))

    forum_area = assert_exists(doc2.find('table', { 'class' : 'forumline' }))
    new_post   = assert_unique(diff_forum_posts(sp_parse_forum_topic(s, r2, forum_area), original_posts)[0])

    new_posts  = [new_post]
    while other_attachments:
        these_attachments = other_attachments[:3]
        other_attachments = other_attachments[3:]
        sp_post_forum_post(s, url, subject, "(continued)", attachments = other_attachments)
        ri = s.get(url)
        doci = bslxml(ri)
        reply_link = absolute(ri, doci.find('a', { 'class' : 'icon_reply' }).get('href'))

        # NOTE: Separate diffs (rather than one diff at the end) so as to not
        # have to worry about possible corner cases when it comes to post
        # ordering
        forum_area = assert_exists(doci.find('table', { 'class' : 'forumline' }))
        new_post = assert_unique(diff_forum_posts(sp_parse_forum_topic(s, ri, forum_area), original_posts+new_posts)[0])

        new_posts = new_posts + [new_post]

    return new_posts

def sp_post_forum_topic(s, url, subject, text, sort = 'Post', attachments = [], quiet = False):
    r   = s.get(url)
    doc = bs(r)

    post_link = absolute(r, doc.find('a', { 'class' : 'icon_new_topic' }).get('href'))

    other_attachments = attachments[3:]
    attachments = attachments[:3]

    if other_attachments and not quiet:
        print("Warning: Studentportalen does not support attaching more than three files, making multiple posts")

    r2 = sp_do_forum_post(s, post_link, subject, text, attachments, sort = sort)
    doc2 = bslxml(r2)

    try:
        forum_area = assert_exists(doc2.find('table', { 'class' : 'forumline' }))
        new_post = assert_unique(sp_parse_forum_topic(s, r2, forum_area))
    except:
        print("Error in parsing new post...")
        if run_in_debug:
            IPython.embed()

    if other_attachments:
        other_posts = sp_post_forum_post(s, r2.url, subject, '(continued)', attachments=other_attachments, quiet=True)
    else:
        other_posts = []

    # return LocalTopic(subject, sort, new_post['author'], [new_post] + other_posts, SPTopic(subject, sort, new_post['author'], len(extra_posts), r3.url))
    return (r2.url, [new_post] + other_posts)

def sp_apply_forum_topic(s, url, f):
    r   = s.get(url)
    doc = bslxml(r)

    edit_links = doc.find_all('a', { 'class' : 'icon_edit' })
    for edit_link in edit_links:
        ri   = s.get(absolute(r, edit_link.get('href')))
        doci = bslxml(ri)

        subj = doci.find('input', { 'name' : 'subject', 'type' : 'text' }).get('value')
        text = doci.find('textarea', { 'name' : 'message' }).contents[0]#decode_contents()
        sortinput = doci.find('input', { 'name' : 'topic_type', 'type' : 'radio', 'checked' : 'checked' }) or \
                    doci.find('input', { 'name' : 'topic_type', 'type' : 'hidden' })
        sortnum   = sortinput.get('value')
        if sortnum == '0':
            sort = 'Post'
        elif sortnum == '1':
            sort = 'Sticky'
        elif sortnum == '2':
            sort = 'Announce'
        else:
            raise NotImplementedError()

        sp_do_forum_post(s, absolute(r, edit_link.get('href')), subj, f(text), [], sort=sort)

def sp_apply_content_page(s, url, f):
    r = s.get(url)
    doc = bs(r)

    edit_link = absolute(r, doc.find('span', {'class' : 'redigera'}).find('a').get('href'))

    r2 = s.get(edit_link)
    doc2 = bs(r2)

    form = doc2.find('form', { 'name' : 'form' })
    action = absolute(r2, form.get('action'))
    # Probably double check that this is correct
    fields = hidden_form_fields(form.parent.parent)

    old_htmlsv = form.find('textarea', { 'name' : 'htmlContentSv' }).decode_contents()
    old_htmlen = form.find('textarea', { 'name' : 'htmlContentEn' }).decode_contents()

    fields['labelSv'] = form.find('input', { 'name' : 'labelSv' }).get('value')
    fields['labelEn'] = form.find('input', { 'name' : 'labelEn' }).get('value')
    fields['htmlContentSv'] = f(old_htmlsv)
    fields['htmlContentEn'] = f(old_htmlen)

    fields['status'] = form.find('input', { 'name' : 'status', 'checked' : '' }).get('value')

    s.post(action, data=fields)

def sp_forum_pageinate(doc):
    pageinate = doc.find('div', { 'class' : 'pagination' })
    if pageinate:
        # NOTE: Last one is a next-page button
        return [ a.get('href') for a in pageinate.find_all('a')[:-1] ]
    else:
        return []

def sp_get_page(s, url):
    r = s.get(url)
    doc = bslxml(r)

    page = { 'type' : 'Unknown', 'body' : doc, 'request' : r }

    content_area = doc.find('div', { 'class' : 'webpage' })
    if content_area:
        page['type'] = 'Content'
        try:
            edit_link = doc.find('span', {'class' : 'redigera'}).find('a').get('href')
            r2 = s.get(absolute(r, edit_link))
            doc2 = bs(r2)

            # IPython.embed()

            text_sv = pypandoc.convert_text(doc2.find('textarea', { 'name' : 'htmlContentSv'}).decode_contents(),#decode_contents(),
                                            'md', format='html')
            text_en = pypandoc.convert_text(doc2.find('textarea', { 'name' : 'htmlContentEn'}).decode_contents(),#decode_contents(),
                                            'md', format='html')

            page['text_sv'] = text_sv
            page['text_en'] = text_en

            return page
        except:
            print("Warning: Unable to figure out what kind of page is at: {}".format(url))
            return { 'type' : 'Unknown', 'body' : doc }

    file_area = doc.select('.filearea')
    if file_area:
        page['type'] = 'File area'
        page['files'] = sp_parse_file_area(r, assert_unique(file_area))
        return page

    forum_area = doc.find('table', { 'class' : 'forumline' })
    if forum_area:
        page['type'] = 'Forum'

        page['topics'] = []
        for r_ in [r] + [ s.get(absolute(r, x)) for x in sp_forum_pageinate(bslxml(r)) ]:
            forum_area = bslxml(r_).find('table', { 'class' : 'forumline' })
            page['topics'] = page['topics'] + sp_parse_forum_area(r_, forum_area)

        return page

    return page

def download_file(s, f, url):
    block_size = 1024
    r = s.get(url, stream=True)
    total = int(r.headers.get('content-length', 0))//block_size
    for chunk in tqdm(r.iter_content(chunk_size=block_size), total=total, unit='kb', unit_scale=False):
        if chunk:
            f.write(chunk)

class LocalFile(Item):
    def __init__(self, name, mtime, path, temporary = False):
        super().__init__(name)
        self._name = name
        self._path = path
        # TODO: Ugly hack, figure out better semantics
        self._temporary = temporary

    def __del__(self):
        if self._temporary:
            os.remove(self._path)
        # super().__del__()

    def sort(self):
        return "file"

    def save(self, where, selector=pick_all):
        print("Writing {}".format(self.title()))
        name = new_name(where, self._name)
        path = os.path.join(where, name)
        shutil.copy2(self._path, path)
        return { path : self }

class SPFile(RemoteItem):
    def __init__(self, name, size, url, mtime):
        super().__init__(name)
        self._size = size
        self._url = url
        self._mtime = mtime
        self._name = name

    def sort(self):
        return "file"

    def sync(self, s):
        print("Downloading {}...".format(self.title()))
        f_, path = tempfile.mkstemp(prefix=self._name, text=False)
        f = os.fdopen(f_, 'wb')
        try:
            download_file(s, f, self._url)
        finally:
            f.close()
        return LocalFile(self._name, self._mtime, path, temporary = True)

class SPFolder(RemoteBrowsable):
    def __init__(self, name, size, url, mtime):
        super().__init__(name)
        self._size = size
        self._url = url
        self._mtime = mtime

    def sort(self):
        return "folder"

    def sync(self, s):
        page = sp_get_page(s, self._url)
        t = page['type']
        if t == 'File area':
            folders = [ SPFolder(folder['name'], folder['size'], folder['url'], folder['updated'])
                        for folder in page['files'] if folder['folder'] ]
            files   = [ SPFile(file_['name'], file_['size'], file_['url'], file_['updated'])
                        for file_ in page['files'] if not file_['folder'] ]
            groups = []
            if folders:
                groups = groups + [('folders', folders)]
            if files:
                groups = groups + [('files', files)]
            return LocalFolder(self.title(), groups, self)
        else:
            raise NotImplementedError()

def dir_name(path):
    if not os.path.exists(path) or not os.path.isdir(path):
        raise RuntimeError()
    l,r = os.path.split(path)
    if r == "":
        return os.path.split(l)[1]
    else:
        return r

def load_course(path):
    stuff = os.listdir(path)

    files = [ thing
              for thing in stuff
              if os.path.isfile(os.path.join(path, thing)) and not thing.startswith('.') ]

    pages = []
    for f in files:
        try:
            p = load_page(os.path.join(path, f))
            print("Loading {} as content page".format(f))
            pages.append(p)
        except:
            IPython.embed()
            print("Unable to load {}, skipping".format(f))

    dirs = [ thing for thing in stuff
             if os.path.isdir(os.path.join(path, thing)) and not thing.startswith('.') ]

    fora = []
    file_area = []
    for d in dirs:
        try:
            p = load_page(os.path.join(path, d))
        except:
            p = None
        if p:
            print("Loading {} as a 'content page'".format(d))
            pages.append(p)
            continue

        try:
            f = load_forum(os.path.join(path, d))
        except:
            f = None
        if f:
            print("Loading {} as a forum".format(d))
            fora.append(f)
            continue

        print("Loading {} as a file area".format(d))
        file_area.append(load_directory(os.path.join(path, d)))

    # def __init__(self, title, items, remote = None):
    items = []
    if fora:
        items.append(("Forums", fora))
    if file_area:
        items.append(("Document", file_area))
    if pages:
        items.append(("Content", pages))
    return LocalCourse(dir_name(path), items)

def load_file(path):
    if not os.path.exists(path):
        print("Error: {} not found".format(path))
        return
    if not os.path.isfile(path):
        print("Error: {} is not a file".format(path))
        return

    name = os.path.basename(path)
    full_path = os.path.abspath(path)

    try:
        # TODO: Figure out better semantics for full_path (should it be a remote=?)
        return LocalFile(name, os.path.getmtime(full_path), full_path, temporary = False)
    # NOTE: I think this is raised every once in a while due to some name escaping issue
    except FileNotFoundError:
        print("Warning: Failed to get {}".format(fpath))

def index_where(xs, p):
    try:
        return next(i for i,x in enumerate(xs) if p(x))
    except:
        return None

def load_directory(path):
    if not os.path.exists(path):
        print("Error: {} not found".format(path))
        raise RuntimeError()
    if not os.path.isdir(path):
        print("Error: {} is not a directory".format(path))
        raise RuntimeError()

    full_path = os.path.abspath(path)

    backref = { }
    for r,ds,fs in os.walk(path):
        rname = dir_name(r)
        # full_rpath = os.path.normpath(os.path.join(full_path, r))
        here = backref.setdefault(r, LocalFolder(rname, [], remote=StoredFolder(os.path.abspath(r))))
        for f in fs:
            fpath = os.path.join(r, f)
            ##
            file_index = index_where(here._items, lambda x: x[0] == 'files')
            if not file_index:
                file_index = len(here._items)
                here._items.append(('files', []))
            here._items[file_index][1].append(load_file(fpath))
        for d in ds:
            dpath = os.path.join(r, d)
            # full_dpath = os.path.join(full_rpath, d)
            dhere = LocalFolder(d, [], remote=StoredFolder(os.path.abspath(dpath)))
            backref[dpath] = dhere
            ##
            dir_index = index_where(here._items, lambda x: x[0] == 'folders')
            if not dir_index:
                dir_index = len(here._items)
                here._items.append(('folders', []))
            here._items[dir_index][1].append(dhere)

    return backref[path]

def load_page(path):
    p_en = frontmatter.load(os.path.join(path, "en"))
    p_sv = frontmatter.load(os.path.join(path, "se"))

    html_sv = pypandoc.convert_text(p_sv.content, 'html', format='md')
    html_en = pypandoc.convert_text(p_en.content, 'html', format='md')

    # TODO: at some point I need to store separate English and Swedish titles
    # IPython.embed()
    return LocalContentPage(p_sv['title'], html_sv, html_en, remote=StoredFolder(os.path.abspath(path)))

def parse_post(path):
    post        = frontmatter.load(path)
    dn,bn       = os.path.split(path)
    parsed_path = re.match("^([0-9]+)( *-+ *)?(.*)$", bn)

    if not 'index' in post.metadata and parsed_path:
        post['index'] = parsed_path.group(1)
    if not 'subject' in post.metadata and parsed_path:
        post['subject'] = parsed_path.group(3)

    attachments = [ load_file(os.path.join(dn, f))
                    for f in post.metadata.setdefault('attachments', []) ]

    return Post( post.metadata.setdefault('subject') or 'no subject',
                 post.metadata.setdefault('index')   or 0,
                 post.metadata.setdefault('date')    or 'unknown',
                 post.metadata.setdefault('author')  or 'unknown',
                 post.content,
                 attachments )

def load_topic(path):
    if os.path.isfile(path):
        # single post without attachments
        # TODO: Add support for other post types
        post = parse_post(path)
        return LocalTopic(subject=os.path.basename(path), author=post._author, sort='Post', posts=[post], remote=StoredFile(os.path.abspath(path)))
    else:
        posts = [ parse_post(os.path.join(path, entry))
                  for entry in os.listdir(path)
                  if not entry.startswith(".") and os.path.isfile(os.path.join(path, entry)) ]
        if len(posts) >= 1:
            try:
                first_post = assert_unique([ post for post in posts if post._index == min([post_._index for post_ in posts if post_._index > 0]) ])
            except:
                first_post = posts[0]
        else:
            print("Error, cannot have empty forum topic: {}".format(path))
            raise NotImplementedError()

        return LocalTopic(subject=dir_name(path), author=first_post._author, sort='Post', posts=posts, remote=StoredFolder(os.path.abspath(path)))

def load_forum(path):
    if not os.path.isdir(path):
        raise NotImplementedError()

    topics = [ load_topic(os.path.join(path,entry))
               for entry in os.listdir(path)
               if not entry.startswith(".") ]

    # TODO: Add support for non-'post' posts
    return LocalForum(title=dir_name(path), posts=[('Post', topics)], remote=StoredFolder(os.path.abspath(path)))

# NOTE: This is a bit hacky T_T
class StoredFile:
    def __init__(self, full_path):
        self._full_path = full_path

class StoredFolder:
    def __init__(self, full_path):
        self._full_path = full_path

class LocalFolder(LocalBrowsable):
    def __init__(self, title, files, remote = None):
        super().__init__(title, files)
        self._remote = remote

    def sort(self):
        return "file area"

    def put(self, s, other):
        if not self._remote or not isinstance(self._remote, SPPage):
            print("Error: Only know how to upload to Studentportalen, skipping {}".format(self.title()))
            return []
        if not (isinstance(other, LocalFolder) or isinstance(other, LocalFile)) :
            print("Can only put files/folders in a file area, skipping {} ({})".format(other.title(), other.sort()))
            return []

        print("Uploading {}...".format(other.title()))
        if isinstance(other, LocalFolder):
            folder_url = sp_make_folder(s, self._remote._url, other.title())
            other_new_remote = SPPage(other.title(), folder_url, 'draft').sync(s)

            all_put = [ (other, other_new_remote) ]
            for item in other.flat_ls():
                all_put = all_put + other_new_remote.put(s, item)

            return all_put
        else:
            # def sp_upload_file(s, url, path):
            sp_upload_file(s, self._remote._url, other._path)
            # TODO: files are not kept track of
            return []

    # TODO: This is an ugly hack, figure out good semantics
    def remote_url(self):
        if self._remote and self._remote._url:
            return self._remote._url

    def force_sync(self, s):
        return self._remote.force_sync(s)

def sp_parse_forum_topic(s, r, forum_area):
    posts = []
    for post_idx,post in enumerate(chunksof(forum_area.find_all('tr', recursive=False), 4, 1, 1)):
        post_date   = post[0].find('div', {'class':'date'}).text.strip()
        post_main   = post[1].find_all('td')
        post_author = post_main[0].text.strip()
        post_body   = post_main[1]

        edit_url = post[0].find('a', { 'class' : 'icon_edit' })
        if not edit_url:
            # We do not have access to the edit page, do the best we can directly with the page...
            # print("Parsing (w/o edit button...")
            # IPython.embed()
            post_subj   = re.sub('^((Subject: )|(Rubrik: ))', '', post[0].find('div', {'class':'subject'}).text.strip())
            post_text   = pypandoc.convert_text(post_body.find('div', { 'class' : 'edit_area' }).decode_contents(),
                                                'md', format='html')
        else:
            r2   = s.get(absolute(r, edit_url.get('href')))
            doc2 = bs(r2)

            # print("Parsing (w edit button...")
            # IPython.embed()

            post_subj = doc2.find('input', { 'name' : 'subject' }).get('value')
            post_text = doc2.find('textarea', { 'name' : 'message' }).contents[0]

            # This is a bit broken...
            if not doc2.find('input', { 'name' : 'disable_bbcode' }).get('checked'):
                post_text = bb.format(post_text)

            post_text = pypandoc.convert_text(post_text, 'md', format='html')

        post_attachments = []
        for attachment_table in post_body.find_all('table', { 'class' : 'attachtable' }):
            attachment_rows  = attachment_table.find_all('tr')

            attachment_desc  = attachment_rows[1].find_all('td')[1].text.strip()
            attachment_size  = attachment_rows[2].find_all('td')[1].text.strip()

            attachment_main  = attachment_rows[0].find_all('td')
            attachment_name  = attachment_main[1].text.strip()
            attachment_url   = absolute(r, attachment_main[2].find('a').get('href'))

            post_attachments = post_attachments + [ { 'description' : attachment_desc,
                                                      'size' : attachment_size,
                                                      'name' : attachment_name,
                                                      'url' : attachment_url } ]

        posts = posts + [{ 'subject' :     post_subj,
                           'index' :       post_idx,
                           'date' :        post_date,
                           'author' :      post_author,
                           'text' :        post_text,
                           'attachments' : post_attachments }]
    return posts

def sp_get_forum_topic(s, url):
    r = s.get(url)
    doc = bslxml(r)

    forum_area = assert_exists(doc.find('table', { 'class' : 'forumline' }))

    return sp_parse_forum_topic(s, r, forum_area)

class Post(Item):
    def __init__(self, subject, index, date, author, text, attachments):
        super().__init__(subject)
        self._subject            = subject
        self._date               = date
        self._index              = index
        self._author             = author
        self._text               = text
        self._attachments        = attachments
        self._cached_attachments = len(attachments) == 0

    def sort(self):
        return "forum post"

    # NOTE: These don't do exactly what one expects, the problem is that I am
    # not sure a post has a stable identity (it doesn't correspond to a URI and
    # order of posts in a topic may change, there could be some sort of ID
    # associated with it though), so this just (re-)downloads the attachments
    def sync(self, s):
        if not self._cached_attachments:
            return self.force_sync(s)
        else:
            return self

    def force_sync(self, s):
        new_attachments = [ attachment.sync(s) for attachment in self._attachments ]
        return Post(self._subject, self._index, self._date, self._author, self._text, new_attachments)

    # NOTE: This is slightly inconsistent behaviour, a single post is saved as
    # a one-post topic, this violates the, possible, expectation that save-ing
    # an item is always the same as save-ing all its constituents
    def save(self, where, selector=pick_all):
        return LocalTopic(self._subject, 'Post', self._author, [self]).save(where, selector)

class LocalTopic(LocalBrowsable):
    def __init__(self, subject, sort, author, posts, remote = None):
        super().__init__(subject, posts)
        self._sort   = sort
        self._author = author
        self._remote = remote

    def sort(self):
        return "forum topic"

    # TODO: This is an ugly hack, figure out good semantics
    def remote_url(self):
        if self._remote and self._remote._url:
            return self._remote._url

    def put(self, s, other):
        # NOTE: Hacky again...
        if not isinstance(self._remote, SPTopic):
            print("Error: Only know how to upload to Studentportalen, skipping {}".format(self.title()))
            return {}
        if not isinstance(other, Post):
            print("Can only post posts in a forum topic, skipping {} ({})".format(other.title(), other.sort()))
            return {}

        print("Posting {}...".format(other.title()))

        # def change_url_link_filter(key, value, form, meta):
        # HACK: Studentportalen removes invalid URLs in href, so we need to store the path elsewhere
        # TODO: Avoid code duplication
        def unparse_links(key, value, form, meta):
            if key == 'Link':
                attr = value[0]
                url,title = value[2]
                # IPython.embed()
                ## TODO: Make the dummy url more unique to each session
                return pandocfilters.Link( attr,
                                           # TODO: This should be some more
                                           # structure thing, but the
                                           # Studentportalen Forum software
                                           # borks on almost all HTML tags!
                                           [ pandocfilters.Str("_ORIGINAL_LINK_URL_{}_ORIGINAL_LINK_TITLE_{}_ORIGINAL_LINK_CONTENTS_".format(url, title)) ]
                                           + value[1],
                                           ("http://dummy.url", "") )
        html_text = pypandoc.convert_text(pandocfilters.applyJSONFilters([unparse_links],
                                                                         pypandoc.convert_text(other._text, 'json', format='md')),
                                          'html', format='json')
        sp_post_forum_post(s, self._remote._url, other._subject, html_text, attachments = [ f._path for f in other._attachments ])
        # WARNING: Posts don't have their own URLs, any post-processing will not get to see new posts in old topics
        # (though we should avoid posting new stuff into old topics anyway!)
        return []

    def force_sync(self, s):
        return self._remote.force_sync(s)

    def save(self, where, selector=pick_all):
        print("Writing {}".format(self.title()))
        name  = new_name(where, self.title())
        path  = os.path.join(where, name)
        files = { path : self }

        posts = [ x for cat,xs in selector(self.title(), self.ls()) for x in xs ]

        # TODO: Figure out a sensible way to have both self._items (in LocalBrowsable) and _posts...
        has_attachments = any([ bool(post._attachments) for post in posts ])
        in_dir          = len(posts) != 1 or has_attachments

        ## NOTE: It is not entirely clear how to keep track of original URLs
        ## this way, there is something incoherent in the semantics here.
        ##
        ## I'll disable saving to single-file format for now (loading single
        ## file format should still be supported)
        in_dir = True
        if in_dir:
            os.makedirs(path)
        if has_attachments:
            attach_dir_path = os.path.join(path, 'attachments')
            os.makedirs(attach_dir_path)

        # if any([ bool(post._attachments) for post in posts ]):
        #     attach_dir_name = new_name(where, "{}-attachments".format(name))
        #     attach_dir_path = os.path.join(where, attach_dir_name)
        #     os.makedirs(attach_dir_path)

        def write_post(f, post):
            # TODO: Figure out nicer way to access attachments
            attachment_files = []
            if post._attachments:
                for attachment in post._attachments:
                    # TODO: This is rather hacky...
                    saved_attachments = attachment.save(attach_dir_path, selector=selector)
                    files.update(saved_attachments)
                    attachment_path = assert_unique(list(saved_attachments))
                    if not attachment_path.startswith(where):
                        raise NotImplementedError()
                    attachment_path_rel = attachment_path[len(path)+1:]

                    attachment_files.append(attachment_path_rel)
            out                = frontmatter.Post(post._text)
            out['date']        = post._date
            out['subject']     = post._subject
            out['author']      = post._author
            out['attachments'] = attachment_files
            # I get str note bytes problem with .dump
            f.write(frontmatter.dumps(out))

        ## TODO: ibid
        if not in_dir:
            post = posts[0]
            files[path] = post
            with open(path, 'w') as f:
                write_post(f, post)
        else:
            for i,post in enumerate(posts):
                post_name = new_name(path, "{} - {}".format(i+1, post._subject))
                post_path = os.path.join(path, post_name)
                files[post_path] = post
                with open(post_path, 'w') as f:
                    write_post(f, post)

        return files

class SPTopic(RemoteBrowsable):
    def __init__(self, subject, sort, author, responses, url):
        super().__init__(subject)
        self._subject   = subject
        self._sort      = sort
        self._author    = author
        self._responses = responses
        self._url       = url

    def sort(self):
        return "forum topic"

    def sync(self, s):
        # def __init__(self, name, size, url, mtime):
        posts = [ Post(post['subject'], post['index'], post['date'], post['author'], post['text'],
                       [ SPFile(attach['name'], attach['size'], attach['url'], post['date']) for attach in post['attachments']] )
                  for post in sp_get_forum_topic(s, self._url) ]
        return LocalTopic(self._subject, self._sort, self._author, [("posts", posts)], self)

class LocalForum(LocalBrowsable):
    def __init__(self, title, posts, remote = None):
        super().__init__(title, posts)
        self._remote = remote

    def sort(self):
        return "forum"

    def put(self, s, other):
        # NOTE: Hacky again...
        if not self._remote or not isinstance(self._remote, SPPage):
            print("Error: Only know how to upload to Studentportalen, skipping {}".format(self.title()))
            return {}
        if not (isinstance(other, Post) or isinstance(other, LocalTopic)) :
            print("Can only post posts/topics in a forum, skipping {} ({})".format(other.title(), other.sort()))
            return {}

        print("Posting {}...".format(other.title()))

        if isinstance(other, Post):
            first_post  = other
            other_posts = []
        else:
            first_post  = other._items[0]
            other_posts = other._items[1:]

        # def change_url_link_filter(key, value, form, meta):
        # HACK: Studentportalen removes invalid URLs in href, so we need to store the path elsewhere
        # TODO: Avoid code duplication
        def unparse_links(key, value, form, meta):
            if key == 'Link':
                attr = value[0]
                url,title = value[2]
                # IPython.embed()
                return pandocfilters.Link( attr,
                                           # TODO: This should be some more
                                           # structure thing, but the
                                           # Studentportalen Forum software
                                           # borks on almost all HTML tags!
                                           [ pandocfilters.Str("_ORIGINAL_LINK_URL_{}_ORIGINAL_LINK_TITLE_{}_ORIGINAL_LINK_CONTENTS_".format(url, title)) ]
                                           + value[1],
                                           ("http://dummy.url", "") )
        html_text = pypandoc.convert_text(pandocfilters.applyJSONFilters([unparse_links],
                                                                         pypandoc.convert_text(first_post._text, 'json', format='md')),
                                          'html', format='json')

        # html_text = pypandoc.convert_text(..., 'html', format='md')
        topic_url = sp_post_forum_topic(s, self._remote._url, first_post._subject, html_text, other._sort,
                                        [ f._path for f in first_post._attachments ] )[0]
        other_new_remote = SPTopic(other.title(), other._sort, other._author, 0, topic_url).sync(s)
        all_put = [ (other, other_new_remote) ]
        for post in other_posts:
            all_put = all_put + other_new_remote.put(s, post)

        return all_put

    # TODO: This is an ugly hack, figure out good semantics
    def remote_url(self):
        if self._remote and self._remote._url:
            return self._remote._url

    def force_sync(self, s):
        return self._remote.force_sync(s)

class SPPage(RemoteBrowsable):
    def __init__(self, title, url, state):
        super().__init__(title)
        self._url   = url
        self._state = state

    def sort(self):
        return "page"

    def sync(self, s):
        page = sp_get_page(s, self._url)
        t = page['type']
        if   t == 'Unknown':
            return LocalUnknownPage(self.title(), page['body'], self)
        elif t == 'Content':
            return LocalContentPage(self.title(), page['text_sv'], page['text_en'], self)
        elif t == 'File area':
            folders = [ SPFolder(folder['name'], folder['size'], folder['url'], folder['updated'])
                        for folder in page['files'] if folder['folder'] ]
            files   = [ SPFile(file_['name'], file_['size'], file_['url'], file_['updated'])
                        for file_ in page['files'] if not file_['folder'] ]
            groups = []
            if folders:
                groups = groups + [('folders', folders)]
            if files:
                groups = groups + [('files', files)]
            return LocalFolder(self.title(), groups, self)
        elif t == 'Forum':
            topics = [ (cat, [SPTopic(topic['subject'], topic['sort'], topic['author'], topic['responses'], topic['url']) for topic in topics])
                       for (cat, topics) in groupby(page['topics'], key=lambda x: x['sort'])]
            return LocalForum(self.title(), topics, self)
        else:
            raise NotImplementedError()

def get_course(s, url):
    r = s.get(url)
    doc = bs(r)

    menu_sections = doc.select('div#treeMenu > ul > li')

    def split_item(item):
        item_url   = absolute(r, item.get('href'))
        item_parts = item.find_all('span')
        item_name  = item_parts[0].text.strip()
        item_state = item_parts[1].text.strip()
        return (item_name, item_url, item_state)

    def safe_extract_section_name(section):
        head = section.find('a', recursive=False)
        if head:
            return head.text.strip()
        else:
            return [x for x in section.children if isinstance(x, str) ][0].strip()

    section_titles = [ safe_extract_section_name(section) for section in menu_sections ]
    section_items  = [ [split_item(item) for item in section.select('ul > li > a')]
                       for section in menu_sections ]

    assert_equal_length(section_titles, section_items)

    return list(zip(section_titles, section_items))

class SPCourse(RemoteBrowsable):
    def __init__(self, title, url):
        super().__init__(title)
        self._url = url

    def sort(self):
        return "course"

    def put(self, s, other):
        if isinstance(other, LocalCourse):
            all_put = []
            for thing in other.flat_ls():
                all_put = all_put + self.put(s, thing)
            return all_put

        if isinstance(other, LocalForum):
            url = sp_add_forum(s, self._url, other.title(), draft=True)
        elif isinstance(other, LocalFolder):
            url = sp_add_file_area(s, self._url, other.title(), draft=True)
        elif isinstance(other, LocalContentPage):
            url = sp_add_content_page(s, self._url, other.title(), html=other._text_sv, html_en=other._text_en, draft=True)
        else:
            url = None

        if url:
            # TODO: This is a bit hacky..., since we know the page type beforehand
            print("Uploading {}...".format(other.title()))
            other_new_remote = SPPage(other.title(), url, 'draft').sync(s)
            all_put = [ (other, other_new_remote) ]
            for topic in other.flat_ls():
                all_put = all_put + other_new_remote.put(s, topic)
            return all_put
        else:
            print("Unable to put {} ({}) directly into a course, skipping".format(other.title(), other.sort()))
            return []

    def sync(self, s):
        course_menu = [ (section, [ SPPage(title, url, state) for title,url,state in items ]) for (section, items) in get_course(s, self._url)]
        return LocalCourse(self.title(), course_menu, self)

class LocalCourse(LocalBrowsable):
    def __init__(self, title, items, remote = None):
        super().__init__(title, items)
        self._remote = remote

    def put(self, s, other):
        if not self._remote:
            print("Error: Not a remote course, skipping {}".format(self.title()))
            return []
        elif not isinstance(self._remote, SPCourse):
            print("Error: Only know how to upload to studentportalen, skipping {}".format(self.title()))
            return []
        else:
            return self._remote.put(s, other)

    def sort(self):
        return "course"

    def load_self(self, path):
        try:
            name = assert_unique(os.listdir(path))
        except:
            return
        if not os.path.isdir(os.path.join(path, name)):
            return
        return load_course(os.path.join(path, name))

    # TODO: This is an ugly hack, figure out good semantics
    def remote_url(self):
        if self._remote and self._remote._url:
            return self._remote._url

    def force_sync(self, s):
        return self._remote.force_sync(s)

def get_course_list(s):
    r   = s.get('https://studentportalen.uu.se')
    doc = bs(r)

    main_area      = assert_unique(doc.select('.currentStudies'))
    section_titles = [ title.text for title in main_area.findAll('h4') ]
    course_lists   = [ [ (course.text, absolute(r, course.get('href'))) for course in lst.findAll('a') ]
                       for lst in main_area.findAll('ul', {'class' : 'courseList'}) ]

    assert_equal_length(section_titles, course_lists)

    return list(zip(section_titles, course_lists))

def load_generic(path):
    return

class SPRoot(RemoteBrowsable):
    def __init__(self):
        super().__init__('Studentportalen')

    def sort(self):
        return "Studentportalen (root)"

    def sync(self, s):
        course_list = [ (title, [ SPCourse(title, url) for (title, url) in lst ]) for (title, lst) in get_course_list(s) ]
        return LocalRoot(course_list, self)

class LocalRoot(LocalBrowsable):
    def __init__(self, courses, remote = None):
        super().__init__('Studentportalen', courses)
        self._remote = remote

    def put(self, s, other):
        print("Error: Cannot put things directly into root!, skipping {}".format(other.title()))
        return []

    def sort(self):
        return "Studentportalen (root)"

    # TODO: This is an ugly hack, figure out good semantics
    def remote_url(self):
        if self._remote and self._remote._url:
            return self._remote._url

    def force_sync(self, s):
        return self._remote.force_sync(s)

class SPrompt(Cmd):
    intro            = "Logged in to Studentportalen..."
    _timestamp       = 0
    _dirty_timestamp = 0
    _history         = []

    def synced_state(self):
        if self._timestamp < self._dirty_timestamp:
            self._state     = self._state.force_sync(self._session)
            self._timestamp = self._dirty_timestamp
        else:
            self._state     = self._state.sync(self._session)
        return self._state

    def deep_synced_state(self):
        self._state = self._state.deep_sync(self._session)
        return self._state

    def do_ls(self, args):
        """List items"""
        state = self.synced_state()

        all_items = state.ls()

        groupless_items = [ item for (section, items) in all_items if section == "" for item in items ]
        groups = [ (section, items) for (section, items) in all_items if section != "" ]

        for item in groupless_items:
            print(item.title())

        for (section_title, items) in groups:
            print("{}: ".format(section_title))
            for item in items:
                print("\t{}".format(item.title()))
        return False

    def push_state(self, state):
        self._history = self._history + [(self._timestamp, self._state)]
        self._state   = state

    def pop_state(self):
        ts,s = self._history.pop()
        self._timestamp = ts
        self._state     = s

    def dirty(self):
        self._dirty_timestamp = self._dirty_timestamp + 1

    def do_cd(self, args):
        """Go to item (or root without arguments). Use .. for parent"""
        state = self.synced_state()

        if not args:
            self._state   = self._root
            self._history = []
        elif args == "..":
            if self._history:
                self.pop_state()
        else:
            matching_items = [ (cat, item) for cat,items in state.ls() for item in items
                               if fuzzy_match(args, "{} {}".format(cat, item.title())) ]
            matching_browsable = [ (cat, item) for cat,item in matching_items if item.is_browsable() ]

            if len(matching_browsable) == 1:
                self.push_state(matching_browsable[0][1])
            elif len([x for cat,x in matching_browsable if x.title() == args]) == 1:
                # Ambiguous with exactly one exact match
                self.push_state([x for cat,x in matching_browsable if x.title() == args][0])
            elif len(matching_browsable) > 1:
                item_text, item_index = pick.pick(['Cancel'] + [ "{}: {}".format(cat, x.title()) for cat,x in matching_browsable ], "Ambiguous, please pick:")
                if item_index > 0:
                    self.push_state(matching_browsable[item_index-1][1])
            elif len(matching_items) == 0:
                print("Not found: {}".format(args))
            else:
                print("None of the following are browsable:")
                for item in matching_items:
                    print(item.title())
        return False

    # def complete_cd(self, text, line, sidx, eidx):
    #     state = self._state
    #     all_items = [ item for (section, items) in state.ls() for item in items ]
    #     matching_items = [ item.title() for item in all_items if (text.lower() in item.title().lower()) and item.is_browsable() ]
    #     return matching_items

    def do_getall(self, args):
        """Downloads current object (completely)"""
        return self.run_get(args, selector=pick_all)

    def do_getsome(self, args):
        """Selectively download parts of current object"""
        print("Warning: This will still download all files (just save selectively)")
        return self.run_get(args, selector=pick_manual)

    def do_get(self, args):
        """Downloads current object (completely)"""
        return self.run_get(args, selector=pick_all)

    def run_get(self, args, selector):
        if args == "":
            args = new_name(".", "save")
        elif os.path.exists(args):
            print("{} already exists! Download cancelled".format(args))
            return
        with tempfile.TemporaryDirectory() as tmpdir:
            print("Downloading to temporary directory: {} (get ready to wait...)".format(tmpdir))
            state = self.deep_synced_state()
            new_files = state.save(tmpdir, selector=selector)

            def is_local_copy(item):
                try:
                    return item._remote._url
                except AttributeError:
                    return False

            def new_path(path):
                if path.startswith(tmpdir):
                    return path[len(tmpdir):]
                else:
                    raise NotImplementedError()

            # TODO: Figure out a less ad-hoc way of doing this

            ## TODO: We should really make the URL/URIs proper types...
            def normalise_sp_url(url):
                try:
                    uri = url[2]
                    if uri[0] == 'forum_post':
                        forum_post_tag,post_num,topic_uri = uri
                        new_uri = ('forum_topic', topic_uri)
                        ## Nice hack.............. not
                        url2 = list(url)
                        url2[2] = new_uri
                        return tuple(url2)
                    else:
                        return url
                except:
                    return url

            translation_table = { normalise_sp_url(parse_sp_url(item._remote._url)) : new_path(path) for path,item in new_files.items() if is_local_copy(item) }
            translation_table_student = {}
            for path,item in new_files.items():
                if is_local_copy(item):
                    try:
                        translation_table_student[normalise_sp_url(parse_sp_url(force_student_url(item._remote._url)))] = new_path(path)
                    except:
                        if run_in_debug:
                            print("Unable to turn into student url: {}".format(item._remote._url))
                            IPython.embed()
            # IPython.embed()

            ## NOTE: Also matches anchors
            urlre = re.compile('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+(?:#((?:[0-9a-zA-Z!$&\'()*+,;=\-._~:@/?]|(?:%[0-9a-fA-F]{2}))*))?')

            def replace_url(urlRaw):
                url = normalise_sp_url(parse_sp_url(urlRaw))
                if url in translation_table:
                    return urllib.parse.quote(translation_table[url])
                url_student = normalise_sp_url(parse_sp_url(force_student_url(urlRaw)))
                if url_student in translation_table_student:
                    return urllib.parse.quote(translation_table_student[url])

                if run_in_debug:
                    print("Not a (known) page: {}".format(urlRaw))
                    IPython.embed()
                return urlRaw

            def replace_urls(data):
                # TODO: This removes one token too much at the end...
                return ''.join(apply_seqs(matches(urlre, data), data, replace_url))

            def replace_urls_in_file(path):
                with open(path, 'r') as f:
                    original_data = f.read()
                with open(path, 'w') as f:
                    f.write(replace_urls(original_data))

            def change_url_str_filter(key, value, form, meta):
                if key == 'Str':
                    return pandocfilters.Str(replace_urls(value))

            def change_url_link_filter(key, value, form, meta):
                if key == 'Link':
                    return pandocfilters.Link( value[0],
                                               pandocfilters.walk(value[1], change_url_str_filter, form, meta),
                                               [ replace_urls(url) for url in value[2] ] )

            def replace_urls_in_markdown(path):
                post_old = frontmatter.load(path)
                post_new = frontmatter.Post(
                    pypandoc.convert_text(
                        pandocfilters.applyJSONFilters([change_url_link_filter],
                                                       pypandoc.convert_text(post_old.content, 'json', format='md')),
                        'md', format='json'))
                post_new.metadata = post_old.metadata
                with open(path, 'w') as f:
                    f.write(frontmatter.dumps(post_new))

            for path,item in new_files.items():
                # if type(item) is LocalTopic:
                #     # TODO: Figure out a nicer way to handle this!
                #     if os.path.isfile(path):
                #         replace_urls_in_markdown(path)
                #     else:
                if type(item) is Post:
                    # replace_urls_in_file(path)
                    replace_urls_in_markdown(path)
                elif type(item) is LocalContentPage:
                    replace_urls_in_markdown(os.path.join(path, 'en'))
                    replace_urls_in_markdown(os.path.join(path, 'se'))

            print('Done')
            shutil.copytree(tmpdir, args)
            self._last_get = args
            print("Stored in: {}".format(args))
        return False

    def do_put(self, args):
        """Put data from local path into current location (default to last save)"""
        if args == "":
            args = self._last_get

        s = self.synced_state()
        other = s.load_self(args) or load_generic(args)
        if not other:
            print("Error: Unable to load {}".format(args))
        else:
            uploaded = s.put(self._session, other)

            base_prefix = os.path.abspath(args)
            translation_table = {}
            missing = []
            for upload in uploaded:
                try:
                    try:
                        local_path = upload[0]._remote._full_path
                        if local_path.startswith(base_prefix):
                            local_path = local_path[len(base_prefix):]
                    finally:
                        # If no local path exists simply ignore the object
                        pass
                    try:
                        translation_table[local_path] = upload[1]._remote._url
                    except:
                        # If no remote URL exists than record it so that we can issue a warning in case it's referenced
                        missing.append(local_path)
                except:
                    pass
            for upload_local, upload_remote in uploaded:
                def apply_multiple(fs):
                    def final(xs):
                        for f in fs:
                            xs = f(xs)
                        return xs
                    return final

                def replace_sp_tags(content):
                    replacements = [("em", "i"), ("strong", "b"), ("span", "p"), ("div", "p")]
                    ## NOTE: Need to use html.parser since it doesn't add <html><body> wrapping
                    bs = BeautifulSoup(content, 'html.parser')
                    for tag_from,tag_to in replacements:
                        if tag_to != "":
                            for t in bs.find_all(tag_from):
                                t.name = tag_to
                        else:
                            for t in bs.find_all(tag_from):
                                r.unwrap()
                    return bs.decode()

                if isinstance(upload_local, LocalForum) and isinstance(upload_remote, LocalForum):
                    pass
                elif isinstance(upload_local, LocalContentPage) and isinstance(upload_remote, LocalContentPage):
                    def reparse_content_urls(content):
                        def reparse_links(key, value, form, meta):
                            if key=="Link":
                                url,c_title = value[2]
                                c_url = urllib.parse.unquote(url)
                                # return None
                                if c_url in translation_table:
                                    c_url_orig = c_url
                                    c_url = force_student_url(translation_table[c_url])
                                    if run_in_debug:
                                        print("New url: {} --> {}".format(c_url_orig, c_url))
                                else:
                                    if run_in_debug:
                                        print("Not a (known) path: {} (ignore this message if this is an actual url)".format(c_url))
                                        IPython.embed()
                                return pandocfilters.Link(value[0], value[1], (c_url, c_title))
                        return pypandoc.convert_text(pandocfilters.applyJSONFilters([reparse_links],
                                                     pypandoc.convert_text(content, 'json', format='html')),
                                                     'html', format='json')

                    sp_apply_content_page(self._session, upload_remote._remote._url, reparse_content_urls)
                elif isinstance(upload_local, LocalTopic) and isinstance(upload_remote, LocalTopic):
                    def reparse_urls(content):
                        # IPython.embed()
                        def reparse_links(key, value, form, meta):
                            if key=="Link":
                                try:
                                    attr = value[0]
                                    url,title = value[2]
                                    if url != "http://dummy.url" or title != "":
                                        raise RuntimeError()
                                    content_head = value[1][0]
                                    if content_head['t'] != 'Str':
                                        raise RuntimeError()
                                    content_tail = value[1][1:]
                                    c_url_qtd,c_title_qtd,c_rest = re.match("^_ORIGINAL_LINK_URL_(.*)_ORIGINAL_LINK_TITLE_(.*)_ORIGINAL_LINK_CONTENTS_(.*)$", content_head['c']).groups()
                                    # NOTE: Yes, double unquote!
                                    c_url = urllib.parse.unquote(urllib.parse.unquote(c_url_qtd))
                                    c_title = urllib.parse.unquote(urllib.parse.unquote(c_title_qtd))
                                    if c_rest == "":
                                        content_orig = containt_tail
                                    else:
                                        content_head['c'] = c_rest
                                        content_orig = [content_head] + content_tail
                                except:
                                    print("WARNING: Found unparsed link")
                                    return None
                                    # return None
                                if c_url in translation_table:
                                    c_url_orig = c_url
                                    c_url = force_student_url(translation_table[c_url])
                                    if run_in_debug:
                                        print("New url: {} --> {}".format(c_url_orig, c_url))
                                else:
                                    if run_in_debug:
                                        print("Not a (known) path: {} (ignore this message if this is an actual url)".format(c_url))
                                        IPython.embed()
                                return pandocfilters.Link(value[0], content_orig, (c_url, c_title))
                        return pypandoc.convert_text(pandocfilters.applyJSONFilters([reparse_links],
                                                     pypandoc.convert_text(content, 'json', format='html')),
                                                     'html', format='json')

                    # IPython.embed()
                    sp_apply_forum_topic(self._session, upload_remote._remote._url, apply_multiple([reparse_urls, replace_sp_tags]))

                    ### remote translation_table[upload[0]._remote._full_path] = upload[1]._remote._url
                    pass
                else:
                    pass
            self.dirty()
        return False

    def do_tryload(self, args):
        s = self.synced_state()
        other = s.load_self(args) or load_generic(args)
        if not other:
            print("Error: Unable to load {}".format(args))
        else:
            print("{}: {}".format(other.sort(), other.title()))
        return False

    def do_debug(self, args):
        """Drops into an IPython debug shell"""
        IPython.embed()
        return False

    def do_goto(self, url):
        self.push_state(SPCourse("Unknown Course", url))
        return False

    def do_exit(self, args):
        """Exit"""
        return True

    def do_quit(self, args):
        """Exit"""
        return True

    def do_logout(self, args):
        """Exit"""
        return True

    def do_EOF(self, args):
        """Exit"""
        return True

def main():
    if os.path.exists(".user") and os.path.isfile(".user"):
        with open('.user', 'r') as f:
            k = json.load(f)
        username = k['username']
        password = k['password']
    else:
        username = input("Username: ")
        password = getpass.getpass("Password: ")

    s = login(username, password)

    root             = SPRoot()
    prompt           = SPrompt()
    prompt._session  = s
    prompt._history  = []
    prompt._state    = root
    prompt._root     = root
    prompt._last_get = 'save'
    prompt.prompt    = "{}> ".format(username)
    prompt.cmdloop()

if __name__ == "__main__":
    main()
